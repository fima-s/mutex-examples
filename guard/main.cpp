#include <mutex>
#include <string>
#include <vector>

std::mutex mutex;
std::vector<std::string> data;

void MutateSharedState() {
  data.push_back("mut");
}

void AvoidThis() {
  mutex.lock();

  MutateSharedState();

  mutex.unlock();
}

bool CondA() {
  return false;
}

bool CondB() {
  return true;
}

void UseLockGuard() {
  std::lock_guard guard(mutex);

  if (CondA()) {
    return;
  }

  MutateSharedState();

  if (CondB()) {
    return;
  }

  MutateSharedState();

}  // <-- mutex.unlock()

int main() {
  AvoidThis();
  UseLockGuard();
  return 0;
}
