#include <twist/ed/stdlike/atomic.hpp>

#include <array>

using twist::ed::stdlike::atomic;

////////////////////////////////////////////////////////////////////////////////

class BrokenMutex1 {
 public:
  BrokenMutex1() {
    want_[0].store(false);
    want_[1].store(false);
  }

  /*
   *  Mutual Exclusion (Safety):
   *  0: want_[0].load() == true
   *    <- 0: want_[0].store(true) // second
   *      <- 1: want_[1].store(true) // first
   */

  // {0, 1}
  void Lock(size_t index) {
    want_[index].store(true);
    while (want_[1 - index].load()) {
      ;  // Backoff
    }
    owner_ = index;
  }

  void Unlock() {
    want_[owner_].store(false);
  }

 private:
  atomic<bool> want_[2];
  size_t owner_;
};

////////////////////////////////////////////////////////////////////////////////

class BrokenMutex2 {
 public:
  void Lock(size_t index) {
    victim_.store(index);
    while (index == victim_.load()) {
      ;  // Backoff
    }
  }

  void Unlock() {
    // Nop
  }

 private:
  atomic<size_t> victim_;
};

////////////////////////////////////////////////////////////////////////////////

class PetersonMutex {
 public:
  PetersonMutex() {
    want_[0].store(false);
    want_[1].store(false);
  }

  /*
   * Mutual Exclusion (Safety):
   * 0: want_[1].load() == true
   *   <- 0: victim_.store(0)  // second
   *     <- 1: victim_.store(1)  // first
   *       <- 1: want_[1].store(true)
   */

  // `index` \in {0, 1}
  void Lock(size_t index) {
    want_[index].store(true);
    victim_.store(index);

    while (want_[1 - index].load() && victim_.load() == index) {
      ;  // Backoff
    }
    // Acquired
    owner_ = index;
  }

  void Unlock() {
    want_[owner_].store(false);
  }

 private:
  atomic<bool> want_[2];
  atomic<size_t> victim_;
  size_t owner_;
};
