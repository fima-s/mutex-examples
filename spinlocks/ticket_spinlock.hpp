#pragma once

#include <twist/ed/stdlike/atomic.hpp>
#include <twist/ed/stdlike/thread.hpp>

#include <cstdlib>

/*
 * counter.fetch_add(delta) ~
 *
 * atomically {
 *   1) old_value <- counter  // read
 *   2) new_value <- old_value + delta  // modify
 *   3) counter <- new_value  // write
 *   return old_value
 * }
 *
 */

class TicketLock {
  using Ticket = size_t;

 public:
  void Lock() {
    Ticket my_ticket = next_free_ticket_.fetch_add(1);

    while (owner_ticket_.load() != my_ticket) {
       ; // Backoff?
    }
  }

  void Unlock() {
    // Do we need atomic increment here?
    owner_ticket_.fetch_add(1);
  }

 private:
  twist::ed::stdlike::atomic<Ticket> next_free_ticket_{0};
  twist::ed::stdlike::atomic<Ticket> owner_ticket_{0};
};
