#pragma once

#include <twist/ed/stdlike/atomic.hpp>
#include <twist/ed/stdlike/thread.hpp>

#include <twist/ed/wait/spin.hpp>

/*
 * atomic.exchange(new_value) ~
 *
 * atomically {
 *    1) old_value <- atomic  // read
 *    2) // modify
 *    3) atomic <- new_value  // write
 *    return old_value
 * }
 *
 */

// Test-and-Set (TAS) spinlock
class TASSpinLock {
 public:
  void Lock() {
    // false -> true
    while (locked_.exchange(true)) {
      // Backoff?
    }
  }

  void Unlock() {
    locked_.store(false);
  }

 private:
  twist::ed::stdlike::atomic<bool> locked_{false};
};
