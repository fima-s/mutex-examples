#include <iostream>
#include <mutex>
#include <thread>
#include <vector>

class ForwardList {
  struct Node {
    Node* next;
  };

 public:
  void Push() {
    Node* new_node = new Node{};
    new_node->next = head_;
    head_ = new_node;
  }

  void Pop() {
    Node* head = head_;
    head_ = head->next;
    delete head;
  }

 private:
  Node* head_{nullptr};
};

void JustWorks() {
  ForwardList list;

  list.Push();
  list.Push();
  list.Pop();
  list.Pop();
  list.Push();
  list.Pop();
}

void ConcurrentAccess() {
  ForwardList list;

  std::vector<std::thread> threads;

  for (size_t i = 0; i < 5; ++i) {
    threads.emplace_back([&] {
      for (size_t j = 0; j < 100'500; ++j) {
        list.Push();
        list.Pop();
      }
    });
  }

  for (auto& t : threads) {
    t.join();
  }
}

int main() {
  JustWorks();
  // ConcurrentAccess();

  return 0;
}
