#include <iostream>

#include <atomic>
#include <thread>
#include <vector>

int main() {
  std::vector<std::thread> threads;

  // Shared
  int counter{0};
  std::atomic<int> atomic_counter{0};

  static const size_t kThreads = 10;
  static const size_t kIncrementsPerThread = 1005001;

  for (size_t i = 0; i < kThreads; ++i) {
    threads.emplace_back([&] {
      for (size_t k = 0; k < kIncrementsPerThread; ++k) {

        // Non-atomic increment

        // Race condition:
        // reg <- mem
        // inc reg
        // reg -> mem

        // Also a data race

        ++counter;

        // Atomic increment
        atomic_counter.fetch_add(1);
      }
    });
  }

  for (auto& t : threads) {
    t.join();
  }

  std::cout << "# Increments made: "
    << kThreads * kIncrementsPerThread << std::endl;

  std::cout << "Shared counter = " << counter << std::endl;
  std::cout << "Shared atomic counter = " << atomic_counter.load() << std::endl;

  return 0;
}
